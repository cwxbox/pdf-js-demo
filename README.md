# PdfJsDemo

## 介绍
这是一个简单的 pdf.js 页面demo程序 你可以在该页面使用 live-sever 插件打开 这个可以让你更加方便测试各种pdf.js版本

## 软件架构
软件架构说明
pdf.js : https://mozilla.github.io/pdf.js/

# 如何创建一套自己的 pdf.js 工具包

## 获取代码

通过git克隆代码

```shell
git clone https://github.com/mozilla/pdf.js.git
cd pdf.js
```

## 下载 NVM

访问此链接 https://github.com/coreybutler/nvm-windows install nvm and use num install node.js (14.18.2) 进行 node.js 版本管理
安装 14.18.2 使用其它版本会报错
```shell
nvm install 14.18.2 
nvm use 14.18.2
```
使用 node -v 检查你当前使用的版本

```shell
node -v
```
## 下载 gulp

接下来下载 gulp 包

```shell
npm install -g gulp-cli
```

如果之前的工作都没有问题那么现在你可以安装 pdf.js 的依赖了

```shell
npm install
```

最后，你需要启动一个本地web服务器，因为一些浏览器不允许使用file:// URL打开PDF文件。运行:

```shell
gulp server
```

然后你可以打开: http://localhost:8888/web/viewer.html

## 创建 PDF.js

为了将所有src/文件捆绑到两个生产脚本中并构建通用查看器，运行:

```shell
gulp generic
```
这将在build/generic/build/目录下生成pdf.js和pdf.worker.js。你可以在那里找到他们。

你可以使用这套工具 来进行你的版本测试 [pdf.js 测试工具](https://download.csdn.net/download/weixin_44849587/86404914)

微信公众号：

![XrX4PI.jpg](https://cwxbox.oss-cn-hangzhou.aliyuncs.com/%E5%BE%AE%E4%BF%A1%E5%85%AC%E4%BC%97%E5%8F%B7%E4%BA%8C%E7%BB%B4%E7%A0%81.jpg)

QQ交流群：

![XrXaVJ.png](https://cwxbox.oss-cn-hangzhou.aliyuncs.com/%E7%BC%96%E7%A8%8B%E4%BA%A4%E6%B5%81%E5%88%86%E4%BA%AB%E7%BE%A4%E7%BE%A4%E8%81%8A%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
